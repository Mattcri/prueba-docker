Iniciamos haciendo Fork del proyecto desde nuestra cuenta de GitLab, para posteriormente clonarla desde nuestro equipo. Una vez descargado el proyecto pasaremos a crear una nueva rama con el comando `git checkout -b task-1` para cambiar y crear nuestra nueva rama **task-1**.

Antes de iniciar a crear nuestra receta Dockerfile, es una buena práctica observar como fue construida la imágen que vamos a utilizar, que para este caso será `nginx:alpine`, ya que queremos una distribución mínima de Linux para lograr nuestro objetivo. 

Muy bien, vayamos a ver la imágen al [repositorio de GitHub](https://github.com/nginxinc/docker-nginx/blob/57f73af83954482c647ac32724a5408bcfc1e7fd/mainline/alpine/Dockerfile). En la parte final del Dockerfile tenemos 2 cosas interesantes para tener en cuenta, que son:
```Dockerfile
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```
La primera línea nos índica que el contenedor estará expuesto en su puerto 80, y la segunda nos permitirá hacer correr nuestro servidor nginx en primer plano.

Perfecto con esta información se nos hará más sencillo construir nuestro **Dockerfile**, procedamos agregando lo necesario para cumplir el objetivo.
```Dockerfile
FROM nginx:alpine
COPY ./src /usr/share/nginx/html
```
Lo que hará este **Dockerfile** será crear una imágen a partir de `nginx:alpine` y tomaremos el contenido del directorio `src` de  nuestro equipo host, en donde tenemos nuestro archivo `html` y lo montaremos en el directorio público del servidor `nginx`.

Ejecutemos el comando necesario para crear nuestra imágen: 

```bash
docker build -t zippy_devops:0.0.1 .
```
Le pasamos el flag `-t` para indicar el tag de nuestra imágen y el punto final en el comando es **muy importante para que Docker reconozca desde que ubicación leer el archivo Dockerfile**

Tenemos creada nuestra imágen, ahora pongámosla a correr el contenedor con: 

```bash
docker run -p 8080:80 -d zippy_devops:0.0.1
```
Con el flag `-p` le indicamos en el primer parámetro en que puerto del equipo host queremos montar el sevicio, para lo solicitado el **8080**, y con el segundo parámetro le indicamos a que puerto del contenedor lo deseamos conectar, como habíamos visto la imágen de `nginx` anteriormente, sabemos que el contenedor esta expuesto en el puerto 80.

Luego con el flag `-d` le diremos que corra el contendor en segundo plano, para ya en la última parte del comando indicarle que imágen queremos tomar para ejecutarla.

Para finalizar esta tarea pusheamos los cambios trabajados en la rama `task-1` a nuestro repositorio y hacemos un merge request desde la interfaz de GitLab. Para efectos prácticos en el merge no eliminaremos la rama cuando se combine con main para dejar evidencia de cada uno de los aspectos trabajados en el laboratorio. 