Para iniciar nuestro contenedor MySQL tomarémos la imágen directamente de Docker Hub y la ejecutaremos:

```bash
docker run -d -p 3306:3306 --name zippy-mysql -e MYSQL_ROOT_PASSWORD=zippyadmin mysql:latest --character-set-server=utf8mb4
```
Nuevamente corremos el contenedor en modo detached y configuramos el puerto.

Con `--name` le damos un nombre a la BBDD. `-e` sirve para darle una variable de entorno y configuramos la contraseña del usuario **ROOT**.

Ocupamos la imágen latest de MySQL, y con `--character-set-server=utf8mb4` configuramos los caracteres que utilizarán por defecto las bases de datos que sean creadas.

---

El siguiente paso será ingresar al contenedor para ejecutar comandos en modo interactivo:

```bash
docker exec -it zippy-mysql mysql -uroot -p
```
Ingresamos posteriormente la contraseña que habíamos configurado como variable de entorno cuando creamos el contenedor: `zippyadmin` y ya podemos trabajar dentro de la instancia mysql del contenedor.

---

Ahora para crear al usuario con su contraseña vamos a necesitar correr algunos comandos SQL

Primero configuramos el nombre de usuario y contraseña:
```sql
CREATE USER 'user_zippy'@'%' IDENTIFIED BY 'somepassword';
```
Se crea el usuario **user_zippy** que podrá conectarse a la BBDD desde cualquier dirección IP, gracias al caracter `%` y con su contraseña respectiva.

Ahora necesitamos otorgar privilegios al usuario que acabamos de crear:
```sql
GRANT ALL PRIVILEGES ON * . * TO 'user_zippy'@'%';
``` 
Le entregamos al usuario permiso para trabajar en todas las BBDD y tablas que se creen en nuestra instancia MySQL.

Y finalmente cargamos los privilegios nuevamente, actualizando caché con:
```sql
FLUSH PRIVILEGES;
```
