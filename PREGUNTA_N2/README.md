Luego de crear una nueva rama para esta tarea, ocuparemos la misma imágen que habíamos creado anteriormente `zippy_devops:0.0.1`, pero en esta ocasión complementaremos con volúmenes para que nuestros cambios realizados desde el host sean escuchados por el archivo montado en el container y se actualicen sin requerir la creación de un nuevo contenedor.

Antes de correr nuestro comando confirmamos que estamos úbicados en la ruta `prueba-docker/PREGUNTA_N2`, y ejecutamos:

```bash
docker run -d -p 8080:80 -v ${PWD}/src:/usr/share/nginx/html zippy_devops:0.0.1
```
Nuevamente le indicamos al contenedor que se ejecute en segundo plano y en el puerto 8080 de nuestro equipo host. Pero en esta ocasión agregamos el flag `-v` para montar un volumen que existirá en nuestro directorio `src` que es donde tenemos los archivos de nuestra aplicación y lo montamos en el directorio público de `nginx`.

¡Excelente! Ahora que tenemos conectado nuestro directorio en host con los archivos que están viviendo en el container, podemos realizar cambios y se verán reflejados en el container. Certifiquemos que estamos en lo correcto y démosle un poco de estilo a nuestro `index` 🎨.

